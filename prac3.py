#!/usr/bin/env python
# coding: utf-8

# In[ ]:


def greet_people(names):
	return ', '.join('Hello ' + name for name in names)


# In[ ]:


def cumulative_sum(lst):
    p = []
    p.append(lst[0])
    for i in range(1,len(lst)):
        q = lst[i]+lst[i-1]
        p.append(q)
    return p
cumulative_sum([1, -2, 3])


# In[ ]:


#Write a program which will find all such numbers which are divisible by 7 but are not a multiple of 5,
#between 2000 and 3200 (both included).
#The numbers obtained should be printed in a comma-separated sequence on a single line.

l = []
for i in range(2000,3201):
    if i%7 == 0 and i%5 != 0:
        l.append(i)
print(''.join(str(l)))


# In[ ]:


q = int(input())
def fact(x):
    if x == 0:
        return 1
    else:
        p = x-1
        return x * fact(p)

print(fact(q))


# In[ ]:


inp = int(input())
dic = {}
for i in range(inp+1):
    dic[i] = i * i
print(dic)


# In[ ]:


p = 34,67,55,33,12,98
q = str(p)
l = q.split(',')
print(q)


# In[ ]:





# In[ ]:


pwd


# In[ ]:


import os
os.list


# In[ ]:


list


# In[ ]:


import os
os.listdir()


# In[ ]:


f = open('tungu.xlsx',r)


# In[ ]:


f = open("tungu")


# In[ ]:


import csv
with open('tungu.xlsx','rt')as f:
  data = csv.reader(f)
  for row in data:
        print(row)


# In[ ]:


import csv
import os
f = open("tungu.xlsx","r")
print(f)


# In[ ]:


f.readline()


# In[ ]:


pwd


# In[ ]:


import os
fd = open("dictpower.txt","w")
fd.write("this is an added line")
print(fd)


# In[ ]:


fd.readline()


# In[ ]:


fd


# In[ ]:


print(fd.read())


# In[ ]:


import os
fd = open("dictpower.txt","w")
fd.write("this is an added line")
fd.readline()


# In[ ]:


print(fd.readline(4))


# In[2]:


tell(fd)


# In[22]:


text = 'abcdefgh'
print(list(text))


# In[ ]:





# In[30]:


num = 10
if num == 10:
    print('true')


# In[31]:


#Let’s say I give you a list saved in a variable: a = [1, 4, 9, 16, 25, 36, 49, 64, 81, 100].
#Write one line of Python that takes this list a and makes a new list that has only the even elements 
#of this list in it.


# In[32]:


a = [1, 4, 9, 16, 25, 36, 49, 64, 81, 100]


# In[33]:


b = []
for i in a:
    if i%2 == 0:
        b.append(i)
print(b)


# In[ ]:




