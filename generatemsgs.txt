names = input("enter your name:").title().split(",")
assignments = input("enter counts of assignments:").split(",")
grades = input("enter ur grades").split(",")

message = "Hi {},\n\nThis is a reminder that you have {} assignments left to \
submit before you can graduate. You're current grade is {} and can increase \
to {} if you submit all assignments before the due date.\n\n"

for name,assignment,grade in zip(names,assignments,grades):
    print(message.format(name,assignment,grade) + int(grade))